#include "engine.h"
#include "orangemonkey_ai.h"
#include "minimaxai.h"

namespace othello
{

bool OthelloGameEngine::initNewGame()
{
    m_board[0].reset();
    m_board[1].reset();

    initPlayerType<HumanPlayer, PlayerId::One>();
    initPlayerType<HumanPlayer, PlayerId::Two>();

    e_playerId = PlayerId::One;

    m_board [0][27] = true;
    m_board [0][36] = true;
    m_board [1][28] = true;
    m_board [1][35] = true;
  return true;
}

bool OthelloGameEngine::initNewGameWithOneAI()
{
    m_board[0].reset();
    m_board[1].reset();

    initPlayerType<HumanPlayer, PlayerId::One>();
    initPlayerType<monkey_ais::OrangeMonkeyAI, PlayerId::Two>();

    e_playerId = PlayerId::One;
    e_playerType = PlayerType::Human;

    m_board [0][27] = true;
    m_board [0][36] = true;
    m_board [1][28] = true;
    m_board [1][35] = true;
  return true;
}

bool OthelloGameEngine::initNewGameWithTwoAI()
{
    m_board[0].reset();
    m_board[1].reset();

    initPlayerType<minimax_ai::MiniMaxAI, PlayerId::One>();
    initPlayerType<monkey_ais::OrangeMonkeyAI, PlayerId::Two>();

//    e_playerId = PlayerId::One;
//    e_playerType = PlayerType::AI;

    m_board [0][27] = true;
    m_board [0][36] = true;
    m_board [1][28] = true;
    m_board [1][35] = true;
  return true;
}


void OthelloGameEngine::clearGame() {
    initNewGame();
}

bool OthelloGameEngine::performMoveForCurrentHuman(const BitPos& board_pos)
{    
    if(utility::isLegalMove(m_board,currentPlayerId(), board_pos))
    {
           utility::placeAndFlip(m_board,currentPlayerId(),board_pos);
           switchCurrentPlayerId();
           return true;
    }
     else
        return false;
}

bool OthelloGameEngine::legalMovesCheck()
{
    return utility::legalMoves(m_board,currentPlayerId()).empty();

}

bool OthelloGameEngine::legalMovesCheckForBoth()
{
    return utility::legalMoves(m_board,currentPlayerId()).empty() &&
           utility::legalMoves(m_board,utility::oppositePlayer(currentPlayerId())).empty();

}


void OthelloGameEngine::switchCurrentPlayerId() {
    if(e_playerId == PlayerId::One)
        e_playerId = PlayerId::Two;
    else
        e_playerId = PlayerId::One;
}

void OthelloGameEngine::think(const std::chrono::seconds& time_limit)
{
    if (currentPlayerType() != PlayerType::AI)
        return;

    // if current player is player 1
    if(e_playerId == PlayerId::One) {
      m_player_one.obj->think(m_board, currentPlayerId(), time_limit);
      utility::placeAndFlip(m_board,currentPlayerId(),m_player_one.obj->bestMove());
    }
    else
    // if current player is player 2
    { m_player_two.obj->think(m_board, currentPlayerId(), time_limit);
        utility::placeAndFlip(m_board,currentPlayerId(),m_player_two.obj->bestMove());
    }

           switchCurrentPlayerId();


}

PlayerId OthelloGameEngine::currentPlayerId() const
{

  return e_playerId;
}

PlayerType OthelloGameEngine::currentPlayerType() const
{
    if(e_playerId == PlayerId::One)
        return m_player_one.type;
    else
        return m_player_two.type;
}

BitPieces OthelloGameEngine::pieces(const PlayerId& player_id) const
{
  return m_board[size_t(player_id)];
}

const BitBoard& OthelloGameEngine::board() const {

    return m_board;
}


}   // namespace othello
