#include "utility.h"

// stl
#include <numeric>
#include <iostream>
#include <functional>
#include <algorithm>
namespace othello::utility
{
  ////////////////////
  //
  //
  // Interface Utility
  // Functions
  //
  //
  ////////////////////

  BitPieces occupiedPositions(const BitBoard& board)
  {
     auto occupiedPositions = BitPieces {board[0] | board[1]};

     return occupiedPositions;
  }

  //checks if a position is occupied in a board
  bool occupied(const BitPieces& pieces, const BitPos& board_pos)
  {
    return pieces.test(board_pos.value());
  }

  //checks if a position is occupied in both boards
  bool occupied(const BitBoard& board, const BitPos& board_pos)
  {
      const auto occupiedPieces = occupiedPositions(board);

    return occupied(occupiedPieces, board_pos);
  }

  BitPos nextPosition(const BitPos& board_pos, const MoveDirection& dir)
  {
      switch (dir) {
      case othello::MoveDirection::N:

          if(board_pos.value() + 8 < othello::detail::computeBoardSize()-1)
          return  othello::BitPos(board_pos.value()+8);
          else return othello::BitPos(board_pos.invalid());
          break;

          case othello::MoveDirection::S:
          if(board_pos.value() > 7)
          return othello::BitPos(board_pos.value()-8);
          else return othello::BitPos(board_pos.invalid());
          break;

          case othello::MoveDirection::E:
          if((board_pos.value()-1) % 8 != 7)
          return othello::BitPos(board_pos.value()-1);
          else return othello::BitPos(board_pos.invalid());
          break;

          case othello::MoveDirection::W:
          if((board_pos.value() + 1) % 8 != 0)
          return othello::BitPos(board_pos.value()+1);
          else return othello::BitPos(board_pos.invalid());
          break;

          case othello::MoveDirection::NE:
          if(board_pos.value() + 7 < othello::detail::computeBoardSize()-1 && (board_pos.value()+7) % 8 != 7)
          return othello::BitPos(board_pos.value()+7);
          else return othello::BitPos(board_pos.invalid());
          break;

          case othello::MoveDirection::NW:
          if(board_pos.value() + 9 < othello::detail::computeBoardSize()-1 && (board_pos.value() + 9) % 8 != 0)
          return othello::BitPos(board_pos.value()+9);
          else return othello::BitPos(board_pos.invalid());
          break;

          case othello::MoveDirection::SE:
          if(board_pos.value() > 7 && (board_pos.value()+7) % 8 != 7)
          return othello::BitPos(board_pos.value()-9);
          else return othello::BitPos(board_pos.invalid());
          break;

          case othello::MoveDirection::SW:
          if(board_pos.value() > 7 && (board_pos.value()-7) % 8 != 0)
          return othello::BitPos(board_pos.value()-7);
          else return othello::BitPos(board_pos.invalid());
          break;

         default:
          return othello::BitPos(board_pos.invalid());

      }
  }

  BitPos findBracketingPiece(const BitBoard& board, const BitPos& board_pos, const PlayerId& player_id, const MoveDirection& dir)
  {
      const auto directionalValue = nextPosition(board_pos, dir);
      othello::BitPos updatedDirectonalValue = nextPosition(directionalValue, dir);

      //checks if first value in direction dir is occupied by opponent
      if(!directionalValue.isValid())
          return BitPos{};
      if(occupied(board[size_t(oppositePlayer(player_id))],directionalValue))
      {

          while(updatedDirectonalValue.isValid())
          {
              if(occupied(board[size_t(player_id)],updatedDirectonalValue))
                  return updatedDirectonalValue;
              if(!occupied(board[size_t(oppositePlayer(player_id))],directionalValue))
                           return BitPos{};
              updatedDirectonalValue = nextPosition(updatedDirectonalValue, dir);
          }
              return BitPos{}.invalid();
      }
       else
          return BitPos{}.invalid();
    }

  //return a list of the possible moves
  BitPosSet legalMoves(const BitBoard& board, const PlayerId& player_id)
  {
    othello::BitPosSet possibleMoves;

    for(size_t i = 0; i < othello::detail::computeBoardSize()-1; i++)
    {
        othello::BitPos currentPos{i};
        if(isLegalMove(board, player_id, currentPos))
            possibleMoves.insert(currentPos);
    }

    return possibleMoves;
  }

  bool isLegalMove(const BitBoard& board, const PlayerId& player_id, const BitPos& board_pos)
  {
      std::vector <othello::MoveDirection> direction  {othello::MoveDirection::N,othello::MoveDirection::S, othello::MoveDirection::E, othello::MoveDirection::W,
                                             othello::MoveDirection::NE, othello::MoveDirection::NW, othello::MoveDirection::SE, othello::MoveDirection::SW};

      if(occupied(board,board_pos))
          return false;
      for (size_t i = 0; i < direction.size();i++)
      {
         auto bracketingPiece = findBracketingPiece(board, board_pos, player_id, direction.at(i));
         if(bracketingPiece.isValid())
         return bracketingPiece.value();
      }

    return false;
  }


  void placeAndFlip(BitBoard& board, const PlayerId& player_id, const BitPos& board_pos)
  {
      std::vector <othello::MoveDirection> direction  {othello::MoveDirection::N,othello::MoveDirection::S, othello::MoveDirection::E, othello::MoveDirection::W,
                                                  othello::MoveDirection::NE, othello::MoveDirection::NW, othello::MoveDirection::SE, othello::MoveDirection::SW};

      std::vector <int> directionValue {8, -8, -1, 1, 7, 9, -9, -7};
      std::vector <size_t> positionsThatNeedsflipping;

      for (size_t i = 0; i < direction.size(); i++)
            {
               auto bracketingPiece = findBracketingPiece(board, board_pos, player_id, direction.at(i));
               auto nextPosition = size_t(int(board_pos.value()) + directionValue[i]);
               int j = 1;
               if(bracketingPiece.isValid())
                   while(!(bracketingPiece.value() - nextPosition == 0))
                   {
                       positionsThatNeedsflipping.push_back(nextPosition);                       
                       j++;
                       nextPosition = size_t(int(board_pos.value()) + directionValue[i]*j);
                   }
            }
       auto & currentPlayer = board[size_t(player_id)];
       auto & opponentPlayer = board[size_t(othello::utility::oppositePlayer(player_id))];

       for (auto piece : positionsThatNeedsflipping)
       {
           currentPlayer.set(piece);
           opponentPlayer.set(piece,false);
       }
       currentPlayer.set(board_pos.value());

  }



  PlayerId oppositePlayer(const PlayerId& player_id){

      if(player_id == PlayerId::One)
          return PlayerId::Two;
      else
          return PlayerId::One;

  }

  int posScore(const BitPos& board_pos, const BitBoard& board, const PlayerId& player_id){

      std::vector <othello::MoveDirection> direction  {othello::MoveDirection::N,othello::MoveDirection::S, othello::MoveDirection::E, othello::MoveDirection::W,
                                                  othello::MoveDirection::NE, othello::MoveDirection::NW, othello::MoveDirection::SE, othello::MoveDirection::SW};
      auto posValue = 0;
      std::vector <int> directionValue {8, -8, -1, 1, 7, 9, -9, -7};
      std::vector <size_t> positionsThatNeedsflipping;

      for (size_t i = 0; i < direction.size(); i++)
            {
               auto bracketingPiece = findBracketingPiece(board, board_pos, player_id, direction.at(i));
               auto nextPosition = size_t(int(board_pos.value()) + directionValue[i]);
               int j = 1;
               if(bracketingPiece.isValid())
                   while(!(bracketingPiece.value() - nextPosition == 0))
                   {
                       positionsThatNeedsflipping.push_back(nextPosition);
                       j++;
                       posValue++;
                       nextPosition = size_t(int(board_pos.value()) + directionValue[i]*j);
                   }
            }

      return posValue;
  }

}   // namespace othello::utility
