#include"minimaxai.h"
#include"utility.h"

#include"iostream"

namespace othello::minimax_ai
{


  MiniMaxAI::MiniMaxAI(const PlayerId& playerId) : m_player_id{playerId}
  {
  }


  void MiniMaxAI::think(const othello::BitBoard& board,
                             const othello::PlayerId& player_id,
                             const std::chrono::seconds& max_time)
  {
    std::cout << "miniMax is thinking!!" << std::endl;


    detail::createTree(m_game_tree,board,player_id,4);

    detail::miniMax(true, *m_game_tree.root);

    bestMove();

//    const auto time_start = std::chrono::steady_clock::now();
  }


  BitPos MiniMaxAI::bestMove() const {

      for (auto const& child : m_game_tree.root->children)
          if(child->data.cost == m_game_tree.root->data.cost)
              m_best_move = child->data.move;

      const auto best_move = BitPos(m_best_move.value());
      m_best_move = BitPos::invalid();
      return best_move;
  }


  void detail::miniMax(bool isMax, detail::BitBoardNode& node){

      if(node.children.empty())
        return;

      // recurrence
      if(isMax)
          node.data.cost = std::numeric_limits<size_t>::min();
      else
          node.data.cost = std::numeric_limits<size_t>::max();

      for (auto const& child : node.children)
          miniMax(!isMax, *child);

      detail::BitBoardNode::ChildContainer::const_iterator elem;
      if(isMax)
          elem = std::max_element(std::cbegin(node.children),
                                            std::cend(node.children),
                                            [](const auto& child_ptr_a, const auto& child_ptr_b){
              return child_ptr_a->data.cost < child_ptr_b->data.cost;
          });
      else
      {
          elem = std::min_element(std::cbegin(node.children),
                                            std::cend(node.children),
                                            [](const auto& child_ptr_a, const auto& child_ptr_b){
              return child_ptr_a->data.cost > child_ptr_b->data.cost;
          });
      }

      node.data.cost = (*elem)->data.cost;
  }

  void detail::computeCostOfMove(BitBoardNode& node, const PlayerId& playerId){

      // base case
      if(node.children.empty()) {
        node.data.cost = node.data.board[size_t(playerId)].count();
        return;
      }

      // recurrence
      for (auto const& childrenAvailable : node.children)
        computeCostOfMove(*childrenAvailable, utility::oppositePlayer(playerId));
  }




  void detail::createChildren(detail::BitBoardNode &parent, const PlayerId& next_player_id, int depth) {

      const auto legal_moves = utility::legalMoves(parent.data.board, next_player_id);

      for(const auto& move : legal_moves)
      {
          othello::BitBoard nodeBoard = parent.data.board;
          utility::placeAndFlip(nodeBoard, next_player_id, move);

          std::unique_ptr<BitBoardNode> node = std::make_unique<detail::BitBoardNode>(nodeBoard);
          node->data.move = move;
          parent.children.push_back(std::move(node));
          if(depth > 0)
          detail::createChildren(*node, utility::oppositePlayer(next_player_id),depth--);
      }

      detail::computeCostOfMove(parent, next_player_id);
  }


  void detail::createTree(detail::BitBoardTree &tree, const BitBoard &board, const PlayerId &maxi_player, int depth)
  {

      tree.root = std::make_unique<detail::BitBoardNode>(board);

      detail::createChildren(*tree.root, maxi_player,depth);
  }
}
   // namespace othello::minimaxs
