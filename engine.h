#ifndef ENGINE_H
#define ENGINE_H



#include "utility.h"

#include <basic_types.h>
#include <engine_interface.h>


namespace othello
{

  class OthelloGameEngine : public othello::GameEngineInterface {

    // GameEngineInterface interface
  public:
    bool initNewGame() override;
    bool initNewGameWithOneAI();
    bool initNewGameWithTwoAI();
    void clearGame() override;
    bool performMoveForCurrentHuman(const othello::BitPos&) override;
    bool legalMovesCheck();
    bool legalMovesCheckForBoth();
    void switchCurrentPlayerId();
    void switchCurrentPlayerType();

    void                think(const std::chrono::seconds&) override;
    othello::PlayerId   currentPlayerId() const override;
    othello::PlayerType currentPlayerType() const override;
    othello::BitPieces  pieces(const othello::PlayerId&) const override;

    const othello::BitBoard& board() const override;

  private : PlayerId e_playerId;
  private: PlayerType e_playerType;

  };

} // namespace othello
#endif   // ENGINE_H
