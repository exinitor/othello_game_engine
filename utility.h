#ifndef UTILITY_H
#define UTILITY_H

#include <utility_interface.h>

namespace othello::utility
{
PlayerId oppositePlayer(const PlayerId& player_id);

int posScore(const BitPos& board_pos, const BitBoard& board, const PlayerId& player_id);

}   // namespace othello::utility

#endif // UTILITY_H
