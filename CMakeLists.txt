# Minimum CMake version required
cmake_minimum_required(VERSION 3.8)

# Global name of the project
project(othello_game_engine VERSION 0.1 LANGUAGES CXX)


# headers and source files
set( HDRS
  orangemonkey_ai.h
  engine.h
  utility.h
  minimaxAI.h
  )

set( SRCS
  orangemonkey_ai.cpp
  engine.cpp
  utility.cpp
  minimaxAI.cpp
  )

add_library( ${PROJECT_NAME} STATIC
  ${HDRS} ${SRCS} )


###################
# Compiler features

# TURN ON C++17 FEATURES
target_compile_features(${PROJECT_NAME} PUBLIC cxx_std_17 )

# Compiler spesific options
target_compile_options(${PROJECT_NAME}
  PUBLIC $<$<CXX_COMPILER_ID:Clang>:
    # Mandatory compiler flags
    -Weverything -Wno-c++98-compat -Wno-c++98-compat-pedantic
    -pedantic -Werror

    # Disable clangs overtidyness for documentation
    -Wno-documentation

    # Deal with Qt - which is not 100% clangified ><
    -Wno-padded
    -Wno-redundant-parens

    # Stuff we want to deal with later
    -Wno-weak-vtables
    >
  PUBLIC $<$<CXX_COMPILER_ID:GNU>:
    # Mandatory compiler flags
    -pedantic -Wall -Werror
    >
    )




##############################
# Configure othello interfaces
find_package(othello_interfaces REQUIRED CONFIG NO_DEFAULT_PATH)
if(othello_interfaces_FOUND)

  # gmlib2 interface_link_libraries
  get_target_property(othello_interfaces_include_directories othello::othello_interfaces INTERFACE_INCLUDE_DIRECTORIES)
  target_include_directories(${PROJECT_NAME} PUBLIC ${othello_interfaces_include_directories})

#  get_target_property(othello_interfaces_link_libraries othello::othello_interfaces INTERFACE_LINK_LIBRARIES)
#  target_link_libraries(${PROJECT_NAME} PUBLIC ${othello_interfaces_link_libraries})

else()
  message(FATAL_ERROR "Missing Othello Interfaces")
endif()











##################################
# Export targets and configuration

target_include_directories( ${PROJECT_NAME} PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}>
)


export(TARGETS ${PROJECT_NAME}
  FILE "${CMAKE_CURRENT_BINARY_DIR}/cmake/${PROJECT_NAME}-targets.cmake"
  NAMESPACE othello::
  )

include(CMakePackageConfigHelpers)
write_basic_package_version_file(
    "${CMAKE_CURRENT_BINARY_DIR}/cmake/${PROJECT_NAME}ConfigVersion.cmake"
    COMPATIBILITY SameMajorVersion
)
configure_package_config_file(
    "cmake/${PROJECT_NAME}-config.cmake.in"
    "${CMAKE_CURRENT_BINARY_DIR}/cmake/${PROJECT_NAME}Config.cmake"
    INSTALL_DESTINATION "lib/cmake/gmlib2qt"
    )
