#ifndef MINIMAXAI_H
#define MINIMAXAI_H

#include <player_interface.h>


namespace othello::minimax_ai
{

    namespace detail {
        struct BitBoardData {
            BitBoard board;
            BitPos move;
            size_t cost = 0;
        };

        struct BitBoardNode {
            using ChildContainer =std::vector<std::unique_ptr<BitBoardNode>>;
            ChildContainer children;
            BitBoardData data;

            BitBoardNode(const BitBoard& board) : data{board} {}

        };


        struct BitBoardTree {
            std::unique_ptr<BitBoardNode> root{nullptr};
        };


        // minimax
        void miniMax(bool isMax, BitBoardNode& node);


        void computeCostOfMove(BitBoardNode& node, const PlayerId& playerId);

        // create tree
        void createTree( BitBoardTree& tree, const BitBoard& board, const PlayerId& maxi_player, int depth);

        void createChildren(BitBoardNode& parent, const PlayerId& next_player_id, int depth);
    }


    class MiniMaxAI : public AIInterface{

    public : MiniMaxAI(const PlayerId& player_id);

    public:
        void think(const BitBoard& board, const PlayerId& player_id,
                   const std::chrono::seconds& max_time);

        BitPos bestMove()const;


    private:
        mutable BitPos m_best_move;
        PlayerId m_player_id;
        detail::BitBoardTree m_game_tree;
        size_t currentBestValue;
    };

}
#endif // MINIMAXAI_H
